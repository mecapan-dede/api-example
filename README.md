# Mecapan API EXAMPLE

This codebase contains Mecapan's API example, implemented with Node.js in TypeScript.

## 🚀 Getting Started for Development

1.  **Install dependencies.**

    With Yarn installed, run the following command:

    ```sh
    yarn install
    ```

2.  **Start the server locally with hot reloading.**

    Run the following command:

    ```sh
    yarn start
    ```
