import { ApolloServer } from "apollo-server";
import { applyMiddleware } from "graphql-middleware";
import { createSchema } from "./schema";

const server = new ApolloServer({
  schema: applyMiddleware(createSchema()),
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
