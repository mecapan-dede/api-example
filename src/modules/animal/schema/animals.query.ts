const { gql } = require("apollo-server");

export const typeDefs = gql`
  type Animal {
    name: String
    age: Int
  }

  extend type Query {
    animals: [Animal]
  }
`;

const animals = [
  {
    name: "Dragon",
    age: 5,
  },
  {
    name: "Whale",
    age: 7,
  },
];

export const createResolvers = () => ({
  Query: {
    animals: () => {
      return animals;
    },
  },
});
