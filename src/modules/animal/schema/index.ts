import * as animalsQuery from "./animals.query";

export const typeDefs = [animalsQuery.typeDefs];

export const createResolvers = () => animalsQuery.createResolvers();
