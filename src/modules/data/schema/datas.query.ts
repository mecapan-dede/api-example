const { gql } = require("apollo-server");

export const typeDefs = gql`
  union Result = Element | Animal

  extend type Query {
    datas: [Result]
  }
`;

const datas = [
  {
    name: "Dragon",
    age: 5,
  },
  {
    name: "Whale",
    age: 7,
  },
  {
    name: "Fire",
    color: "Red",
  },
  {
    name: "Water",
    color: "Blue",
  },
];

interface Data {
  name: String;
  color: String;
  age: Number;
}

export const createResolvers = () => ({
  Result: {
    __resolveType(obj: Data) {
      if (obj.color) {
        return "Element";
      }
      if (obj.age) {
        return "Animal";
      }
      return null;
    },
  },

  Query: {
    datas: () => {
      return datas;
    },
  },
});
