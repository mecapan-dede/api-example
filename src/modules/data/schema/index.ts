import * as datasQuery from "./datas.query";

export const typeDefs = [datasQuery.typeDefs];

export const createResolvers = () => datasQuery.createResolvers();
