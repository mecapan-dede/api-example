const { gql } = require("apollo-server");

export const typeDefs = gql`
  type Element {
    name: String
    color: String
  }

  extend type Query {
    elements: [Element]
  }
`;

const elements = [
  {
    name: "Fire",
    color: "Red",
  },
  {
    name: "Water",
    color: "Blue",
  },
];

export const createResolvers = () => ({
  Query: {
    elements: () => {
      return elements;
    },
  },
});
