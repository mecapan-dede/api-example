import * as elementsQuery from "./elements.query";

export const typeDefs = [elementsQuery.typeDefs];

export const createResolvers = () => elementsQuery.createResolvers();
