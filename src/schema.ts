import { gql } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";
import merge from "lodash.merge";
import {
  createResolvers as createAnimalResolvers,
  typeDefs as animalTypeDefs,
} from "./modules/animal/schema";
import {
  createResolvers as createDataResolvers,
  typeDefs as dataTypeDefs,
} from "./modules/data/schema";
import {
  createResolvers as createElementResolvers,
  typeDefs as elementTypeDefs,
} from "./modules/element/schema";

const rootTypeDefs = gql`
  type Query {
    _root: Boolean
  }
  type Mutation {
    _root: Boolean
  }
`;

const rootResolvers = {
  Query: {
    _root: () => false,
  },
  Mutation: {
    _root: () => false,
  },
};

export const createSchema = () =>
  makeExecutableSchema({
    typeDefs: [
      rootTypeDefs,
      ...animalTypeDefs,
      ...elementTypeDefs,
      ...dataTypeDefs,
    ],
    resolvers: merge(
      rootResolvers,
      createAnimalResolvers(),
      createElementResolvers(),
      createDataResolvers()
    ),
  });
